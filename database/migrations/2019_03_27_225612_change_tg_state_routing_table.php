<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTgStateRoutingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tg_state_routing', function (Blueprint $table) {
            $table->dropColumn('state');
        });

        Schema::table('tg_state_routing', function (Blueprint $table) {
            $table->string('state')->after('chat_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tg_state_routing', function (Blueprint $table) {
            $table->dropColumn('state');
        });

        Schema::table('tg_state_routing', function (Blueprint $table) {
            $table->json('state');
        });
    }
}
