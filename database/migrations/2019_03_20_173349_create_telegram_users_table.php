<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelegramUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_users', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('name');
            $table->enum('role', ['user', 'admin'])->default('user');
            $table->integer('phone');
            $table->string('lang')->default('ru');
            $table->timestamps();
        });

        Schema::create('user_cards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->enum('type', ['uzcard', 'kiwi']);
            $table->integer('number');
            $table->timestamps();
        });

//        Schema::table('user_cards', function (Blueprint $table) {
//            $table->foreign('telegram_user_id')->references('id')->on('telegram_users');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_users');
        Schema::dropIfExists('user_cards');
    }
}
