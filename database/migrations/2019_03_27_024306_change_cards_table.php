<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_cards', function(Blueprint $table) {
            $table->dropColumn('type');
        });

        Schema::table('user_cards', function(Blueprint $table) {
            $table->enum('type', ['uzcard', 'qiwi'])->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('user_cards', function(Blueprint $table) {
            $table->enum('type', ['uzcard', 'kiwi'])->change();
        });*/
    }
}
