<?php

namespace App\Models\User;

use App\Models\Card\Card;
use App\Models\State\State;
use Illuminate\Database\Eloquent\Model;

class TelegramUser extends Model
{
    protected $table = 'telegram_users';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'chat_id',
        'name',
        'role',
        'phone',
        'lang'
    ];

    public function cards()
    {
        return $this->hasMany(Card::class,'user_id');
    }

    public function state()
    {
        return $this->hasOne(State::class,'chat_id', 'chat_id');
    }

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($user) {

            $cards = $user->cards;
            $cards->delete();

        });
    }
}
