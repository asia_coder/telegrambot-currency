<?php

namespace App\Models\Order;

use App\Models\Currency\Currency;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public static $PENDING = "pending";
    public static $PAY = "pay";
    public static $WAIT = "wait";
    public static $ACCEPT = "accept";
    public static $REJECTED = "rejected";

    protected $table = 'order';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'user_id',
        'give_currency',
        'get_currency',
        'give_amount',
        'get_amount',
        'status',
    ];

    public function giveCurrency()
    {
        return $this->hasOne(Currency::class, 'id', 'give_currency');
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::class, 'id', 'get_currency');
    }
}
