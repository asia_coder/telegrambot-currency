<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AdminCard extends Model
{
    protected $table = 'admin_cards';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'card',
        'number',
        'fio'
    ];
}
