<?php

namespace App\Models\State;

use App\Models\User\TelegramUser;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = "tg_state_routing";

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
      'chat_id',
      'state',
    ];

    public function user()
    {
        return $this->belongsTo(TelegramUser::class,'chat_id', 'chat_id');
    }
}
