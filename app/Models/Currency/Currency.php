<?php

namespace App\Models\Currency;

use App\Models\Order\Order;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currencies';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'selling_rate',
        'purchase_rate',
        'currency_amount',
        'currency_en',
    ];

    public function orderGive()
    {
        return $this->belongsTo(Order::class, 'give_currency', 'id');
    }

    public function orderGet()
    {
        return $this->belongsTo(Order::class, 'get_currency', 'id');
    }
}
