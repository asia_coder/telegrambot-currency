<?php

namespace App\Models\Card;

use App\Models\User\TelegramUser;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'user_cards';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'user_id',
        'type',
        'number'
    ];

    public function user()
    {
        return $this->belongsTo(TelegramUser::class,'id', 'user_id');
    }
}
