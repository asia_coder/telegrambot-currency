<?php

namespace App\Http\Controllers\Telegram\Callback;

use App\Http\Controllers\Telegram\Buttons\AdminController;
use App\Http\Controllers\Telegram\Buttons\OrderController;
use App\Http\Controllers\Telegram\User\UserController;
use App\Models\Card\Card;
use App\Models\Currency\Currency;
use App\Models\Data;
use App\Models\Order\Order;
use App\Models\User\TelegramUser;
use App\Services\Keyboards\ReplyKeyboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class CallbackController extends Controller
{
    public static function init($hook)
    {
        try{
            $is_auth = $hook['auth'];
            $callback_query = $hook['callback_query'];
            $chat_id = $callback_query['from']['id'];

            $name = $callback_query['from']['first_name'] ?? 'NO NAME';

            if (strpos($callback_query['data'], "change_lang") !== false) {
                $arr = explode('_' , $callback_query['data']);
                $lang = end($arr);

                if(!$is_auth){
                    $data = [
                        'chat_id' => $chat_id,
                        'lang'  => $lang,
                        'name'  => $name,
                        'phone' => '0',
                        'role'  => 'user'
                    ];
                    (new UserController)->store($data);
                }
                else{
                    $data = [
                        'lang' => $lang
                    ];
                    (new UserController)->update($hook['user_id'], $data);
                }

                Data::setUserLang($lang);

                Telegram::answerCallbackQuery([
                    "callback_query_id" => $callback_query['id']
                ]);

                Telegram::deleteMessage([
                    "chat_id" => $chat_id,
                    "message_id" => $callback_query['message']['message_id']
                ]);

                $msg = ReplyKeyboard::emoji(false)->getKeyboard(
                    $chat_id,
                    Data::getUserConfig('send_phone'),
                    Data::getContactMenu()
                );

                if ($is_auth) {
                    $msg = ReplyKeyboard::emoji(false)->getKeyboard(
                        $chat_id,
                        Data::getUserConfig('text_main_menu'),
                        Data::getMainMenu()
                    );
                }

                try{
                    Telegram::sendMessage($msg);
                } catch(TelegramResponseException $e){
                    return;
                }

            } elseif (strpos($callback_query['data'], "admin_card") === 0){
                (new AdminController())->card($callback_query['data']);
            } elseif (strpos($callback_query['data'], "change_currency_") === 0){
                $arr = explode('_' , $callback_query['data']);

                $state = json_encode([
                    "admin" => [
                        "method" => "setCurrency",
                        "id"     => $arr[2],
                        "type"    => $arr[3]
                    ]
                ]);

                Telegram::sendMessage([
                    "chat_id" => $chat_id,
                    "text" => "Введите новый курс"
                ]);

                Telegram::answerCallbackQuery([
                    "callback_query_id" => $callback_query['id']
                ]);

                Telegram::deleteMessage([
                    "chat_id" => $chat_id,
                    "message_id" => $callback_query['message']['message_id']
                ]);

                Data::setState($state);
            }
            elseif (strpos($callback_query['data'], "change_reserve_") === 0){
                $arr = explode('_' , $callback_query['data']);

                $state = json_encode([
                    "admin" => [
                        "method" => "setCurrencyAmount",
                        "id"     => $arr[2]
                    ]
                ]);

                Telegram::sendMessage([
                    "chat_id" => $chat_id,
                    "text" => "Введите новый резерв"
                ]);

                Telegram::answerCallbackQuery([
                    "callback_query_id" => $callback_query['id']
                ]);

                Telegram::deleteMessage([
                    "chat_id" => $chat_id,
                    "message_id" => $callback_query['message']['message_id']
                ]);

                Data::setState($state);
            }
            elseif (strpos($callback_query['data'], "order_re") === 0){
                $arr = explode('_' , $callback_query['data']);
                $order = Order::where('id', $arr[2])->first();
                $user = TelegramUser::where('id', $order->user_id)->first();

                if (isset($order->status) && $order->status !== Order::$WAIT) {
                    Telegram::deleteMessage([
                        "chat_id" => $chat_id,
                        "message_id" => $callback_query['message']['message_id']
                    ]);

                    $statusText = "Отменен";
                    if ($order->status === Order::$ACCEPT) {
                        $statusText = "Принят";
                    }

                    Telegram::sendMessage([
                        "chat_id" => Data::getChatId(),
                        "text"    => "Заявка по номеру ". $order->id . " уже обработана.\nCтатус: $statusText"
                    ]);
                    return;
                }

                if ($arr[1] == 'resolve') {
                    $order->status = Order::$ACCEPT;
                    $order->save();

                    if ($user->lang === 'uz') {
                        $textOrderStatus = "Buyurtma ID-si: $order->id\n
Sizning buyurtmangiz <b>qabul qilindi</b>
Status: <b>To'lov amalga oshirildi</b>
";
                    } else {
                        $textOrderStatus = "ID заявка: $order->id\n
Ваша заявка <b>одобрена</b>
Статус: <b>Оплачен</b>
";
                    }


                    Telegram::sendMessage([
                        "chat_id" => $user->chat_id,
                        "text"    => $textOrderStatus,
                        "parse_mode" => "html"
                    ]);

                    try {

                        $user = TelegramUser::where('id', $order->user_id)->first();
                        $currencyFrom = Currency::where('id', $order->give_currency)->first();
                        $currencyTo = Currency::where('id', $order->get_currency)->first();

                        $card_type = "";
                        switch ($currencyTo->currency_en) {
                            case Data::$UZCARD:
                                $card_type = "uzcard";
                                break;
                            case Data::$QIWI_USD:
                            case Data::$QIWI_RUB:
                                $card_type = "qiwi";
                                break;
                        }

                        $card = Card::where('user_id', $user->id)->where('type', $card_type)->first();

                        if (!empty($card) && isset($currencyFrom->currency_en) && isset($currencyTo->currency_en)) {
                            $date = date('d.m.Y H:i', strtotime($order->updated_at));
                            $channelToMessage = "🆔 ID: $order->id
👤: $user->name
🔀: $currencyFrom->currency_en ➡️ $currencyTo->currency_en
📆: $date
🔎Status: ✅ 
💰: $order->give_amount $currencyFrom->currency_en ➡️ $order->get_amount $currencyTo->currency_en";

                            usleep(20000);

                            Telegram::sendMessage([
                                "chat_id" => "@crossmarketing_uz",
                                "text"    => $channelToMessage
                            ]);
                        }

                    } catch(TelegramResponseException $e){
                        info($e);
                    }

                }
                elseif ($arr[1] == 'reject') {
                    $order->status = Order::$REJECTED;
                    $order->save();

                    if ($user->lang === 'uz') {
                        $textOrderStatus = "Buyurtma ID-si: $order->id\n
Sizning buyurtmangiz <b>bekor qilindi</b>
Status: <b>To'lov qilinmadi</b>
";
                    } else {
                        $textOrderStatus = "ID заявка: $order->id\n
Ваша заявка <b>отменена</b>
Статус: <b>Не оплачен</b>
";
                    }

                    $currencyFrom = Currency::where('id', $order->give_currency)->first();
                    $currencyTo = Currency::where('id', $order->get_currency)->first();

                    $currencyFrom->currency_amount = $currencyFrom->currency_amount - $order->give_amount;
                    $currencyTo->currency_amount = $currencyTo->currency_amount + $order->get_amount;

                    $currencyFrom->save();
                    $currencyTo->save();

                    Telegram::sendMessage([
                        "chat_id" => $user->chat_id,
                        "text"    => $textOrderStatus,
                        "parse_mode" => "html"
                    ]);

                    usleep(30000);
                }

                Telegram::answerCallbackQuery([
                    "callback_query_id" => $callback_query['id']
                ]);

                Telegram::deleteMessage([
                    "chat_id" => $chat_id,
                    "message_id" => $callback_query['message']['message_id']
                ]);

                Telegram::sendMessage([
                    "chat_id" => $chat_id,
                    "text"    => "Заявка по номеру ". $order->id . " обработана"
                ]);
            } elseif (isset($callback_query['data'])) {
                $request_data = json_decode($callback_query['data'], true);

                if (!empty($request_data) && isset($request_data['order'])) {
                    (new OrderController())->run($request_data['order']);
                }
            }
        }
        catch(TelegramResponseException $e){
            return;
        }
    }
}
