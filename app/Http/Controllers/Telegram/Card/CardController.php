<?php

namespace App\Http\Controllers\Telegram\Card;

use App\Models\Card\Card;
use App\Models\User\TelegramUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CardController extends Controller
{
    public function index(TelegramUser $user)
    {
        return response()->json($user->cards());
    }

    public function store(TelegramUser $user, $data)
    {
        $card = new Card();
        $card->type = $data['type'];
        $card->number = $data['number'];

        $user->cards()->save($card);

        //return response()->json($user, 201);
        return $user;
    }

    public function update(TelegramUser $user, Card $card, $data)
    {
        $card->update($data);

        return response()->json($card, 201);
    }

    public function delete(TelegramUser $user, Card $card)
    {
        $card->delete();
        return response()->json(201);
    }

    public function deleteAll(TelegramUser $user)
    {
        $user->cards()->delete();
        return response()->json(201);
    }
}
