<?php

namespace App\Http\Controllers\Telegram;

use App\Models\Data;
use App\Models\State\State;
use App\Models\User\TelegramUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Exceptions\TelegramOtherException;
use Telegram\Bot\Exceptions\TelegramResponseException;

class RouteController extends Controller
{
    protected $user_request;
    public $state_result;

    public function route($route_request)
    {
        try{
            $user = TelegramUser::find($route_request['user_id']);
            if (isset($route_request['message']['text'])) {
                $this->user_request = $this->parseText($route_request['message']['text']);
            } else {
                return;
            }

            if ( $this->user_request === Data::getUserConfig('back_menu') ) { // Вернуться в главное меню
                $this->getMain();
                return;
            }

            $this->state_result = State::where('chat_id', $user->chat_id)->where('state','!=','')->first();

            try{
                if(Data::getUserConfig('page_buttons')){
                    $buttons = array_flip(
                        Data::getUserConfig('page_buttons')
                    );
                }
                else{
                    return;
                }

            }
            catch(TelegramResponseException $e){
                return;
            }

            $classPrefix = isset($buttons[$this->user_request]) ? $buttons[$this->user_request] : '';
            $controllerName = ucfirst($classPrefix);

            $tgController = $this->getClass($controllerName);

            $isClass = !isset(json_decode($this->state_result['state'], true)[$classPrefix]);

            if ( class_exists($tgController) && ($isClass || $classPrefix == 'order') ) {
                Data::setState();
                $tgPageController = new $tgController;
                try{
                    $tgPageController->page();
                }
                catch(TelegramOtherException $e){
                    info("method page not exists");
                    return;
                }
            } else {
                if (!empty($this->state_result)) {
                    $state = json_decode($this->state_result['state'], true);
                    $controllerName = key($state);
                    $method = isset($state[$controllerName]['method']) ? $state[$controllerName]['method'] : '';
                    $tgController = $this->getClass(ucfirst($controllerName));

                    if (
                            class_exists($tgController) && method_exists($tgController, $method)
                        ) {
                        $tgPageController = new $tgController;
                        $tgPageController->$method($this->user_request);
                    }
                }
            }
        }
        catch(TelegramResponseException $e){
            info($e);
            return;
        }
    }

    protected function getClass($class_prefix)
    {
        return "App\\Http\\Controllers\\Telegram\\Buttons\\" .$class_prefix. "Controller";
    }
    protected function parseText(string $text): string
    {
        return trim($text);
        // return Data::getPattern();
    }

    protected function validUserRequest()
    {
        // $state = json_decode($this->state_result);

    }

    protected function getMain()
    {
        Data::setState();
        Data::getMain(Data::getUserConfig('text_main_menu'));
    }
}
