<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Telegram\Message\MessageController;
use App\Http\Controllers\Telegram\User\UserController;
use App\Models\Data;
use App\Http\Controllers\Telegram\Callback\CallbackController;
use App\Models\User\TelegramUser;
use App\Services\Keyboards\ReplyKeyboard;
use App\Http\Controllers\Controller;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramController extends Controller
{
    public static $hook;
    public static $type = false;
    public static $user;

    public function __construct()
    {
       // $this->hook = Telegram::getWebhookUpdates();
    }

    public function webhook()
    {
        /*if (isset(self::$hook['user_id']) && self::$hook['user_id'] != 11) {
            return;
        }*/

        try {
            if (isset(self::$hook['user_id']) && self::$hook['user_id']) {
                self::$user = TelegramUser::find(self::$hook['user_id']);
            }

            if(self::$user){
                Data::setChatId(
                    intval(self::$user->chat_id)
                );

                Data::setUserLang(
                    self::$user->lang
                );
            }

            if ( isset(self::$hook['message']) ) {

                if (isset(self::$hook['message']['message_id'])) {
                    Data::setMessageId(self::$hook['message']['message_id']);
                }

                self::$type = MessageController::init(self::$hook);

                if (self::$type === 'command' || !self::$hook['auth']) {

                    CommandController::init(self::$hook, true);
                    return;
                }
                elseif (self::$type === 'phone') {

                    if(self::$hook['user_phone']){
                        //тут будет смена номера через настройки

                        //return;
                    }
                    else{
                        $phone = self::$hook['message']['contact']['phone_number'] ?? self::$hook['message']['text'];
                        $phone = str_replace('+', '', $phone);

                        (new UserController())->update(self::$hook['user_id'], ['phone'=>$phone]);

                        $msg = ReplyKeyboard::emoji(false)->getKeyboard(
                            self::$hook['message']['chat']['id'],
                            Data::getUserConfig('text_main_menu'),
                            Data::getMainMenu()
                        );

                        try{
                            Telegram::sendMessage($msg);
                        } catch(TelegramResponseException $e){
                            return;
                        }

                        return;
                    }
                }

                (new RouteController)->route(self::$hook);

            } elseif ( isset(self::$hook["callback_query"]) ) {
                try{

                    if (isset(self::$hook['callback_query']['message']['message_id'])) {
                        Data::setMessageId(
                            self::$hook['callback_query']['message']['message_id']
                        );
                    }

                    CallbackController::init(self::$hook);
                    return;
                }
                catch(TelegramResponseException $e){
                    return;
                }

            }

        } catch (TelegramResponseException $e){
            return;
        }
    }
}
