<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 2019-03-27
 * Time: 02:26
 */

namespace App\Http\Controllers\Telegram\Buttons;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\Card\CardController;
use App\Models\Card\Card;
use App\Models\Data;
use App\Models\User\TelegramUser;
use App\Services\Keyboards\ReplyKeyboard;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class QiwiController extends Controller
{
    public function page()
    {
        $state = json_encode([
            "qiwi" => [
                "method" => "qiwiValidator"
            ]
        ]);

        $cards = Data::getCards();
        $keyboard = [
            [
                $cards['qiwi'],
                $cards['uzcard']
            ],
            [
                Data::getMenuButton()
            ]
        ];

        $text = Data::getUserConfig('qiwi_text_number');

        $msg = ReplyKeyboard::emoji(false)->getKeyboard(
            Data::getChatId(),
            $text,
            $keyboard
        );

        try {
            Telegram::sendMessage($msg);
            Data::setState($state);
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }
    }

    public function qiwiValidator(string $user_request)
    {
        $regexQiwi = Data::getQiwiPattern();

        $cards = Data::getCards();
        $keyboard = [
            [
                $cards['qiwi'],
                $cards['uzcard']
            ],
            [
                Data::getMenuButton()
            ]
        ];

        $text = Data::getUserConfig('error_request');

        if ( $isCorrect = (preg_match($regexQiwi, $user_request) == 1) ) {
            $text = Data::getUserConfig('qiwi_text_save');
        }

        $msg = ReplyKeyboard::emoji(false)->hideKeyboard()->getKeyboard(
            Data::getChatId(),
            $text,
            $keyboard
        );

        try {
            if ($isCorrect) {
                $userObject = new TelegramUser();
                $user = $userObject->where('chat_id', Data::getChatId())->first();

                $cardObject = new Card();
                $qiwiCard = $cardObject->where('user_id', $user->id)
                    ->where('type', 'qiwi')
                    ->first();

                $card = new CardController();
                if (empty($qiwiCard)) {
                    $card->store($user, [
                        "type" => "qiwi",
                        "number" => $user_request
                    ]);
                } else {
                    $card->update($user, $qiwiCard, [
                        "number" => $user_request
                    ]);
                }

                Data::setState();
            }
            Telegram::sendMessage($msg);
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }
    }
}
