<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 2019-03-24
 * Time: 16:07
 */

namespace App\Http\Controllers\Telegram\Buttons;


use App\Http\Controllers\Controller;
use App\Models\Data;
use App\Models\State\State;
use App\Models\User\TelegramUser;
use App\Services\Keyboards\ReplyKeyboard;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class CardsController extends Controller
{
    public function page()
    {
        try {
            $cardsUser = [];
            $user = TelegramUser::where('chat_id', Data::getChatId())->first();
            $cardsUser = $user->cards()->get();

            $text = Data::getUserConfig('your_cards')."\n\n";

            $empty_text = Data::getUserConfig('empty_text');

            $qiwiNumber = "<b>$empty_text</b>";
            $uzCardNumber = "<b>$empty_text</b>";

            foreach($cardsUser as $card) {
                switch ($card['type']) {
                    case 'qiwi':
                        $qiwiNumber = "<b>$card[number]</b>";
                        break;

                    case 'uzcard':
                        $uzCardNumber = "<b>$card[number]</b>";
                        break;
                }
            }

            $qiwiCard = "QIWI:\n" . $qiwiNumber;
            $uzCard = "UZCARD:\n" . $uzCardNumber;

            $text .= $uzCard . "\n\n";
            $text .= $qiwiCard;

            $cards = Data::getCards();
            $keyboard = [
                [
                  $cards['qiwi'],
                  $cards['uzcard']
                ],
                [
                    Data::getMenuButton()
                ]
            ];

            $msg = ReplyKeyboard::emoji(false)->hideKeyboard()->getKeyboard(
                Data::getChatId(),
                $text,
                $keyboard
            );

            Telegram::sendMessage($msg);
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }

    }
}
