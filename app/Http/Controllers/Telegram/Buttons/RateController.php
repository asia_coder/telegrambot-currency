<?php

namespace App\Http\Controllers\Telegram\Buttons;

use App\Models\Currency\Currency;
use App\Models\Data;
use App\Http\Controllers\Controller;
use Telegram\Bot\Laravel\Facades\Telegram;

class RateController extends Controller
{
    public function page()
    {
        $chat_id = Data::getChatId();
        $currencies = Currency::all()->toArray();

        if (Data::getUserLang() === "uz") {
            $text = "Sotib olish:\n";
        } else {
            $text = "Покупка:\n";
        }
        foreach($currencies as $value){
            if($value['currency_en'] != 'UZCARD'){
                $text .= "<strong>" .$value["currency_en"]. "</strong>" . ' = ' . $value["purchase_rate"] . " UZS\n";
            }
        }

        if (Data::getUserLang() === "uz") {
            $text .= "\nSotish:\n";
        } else {
            $text .= "\nПродажа:\n";
        }

        foreach($currencies as $value){
            if($value['currency_en'] != 'UZCARD'){
                $text .= "<strong>" .$value["currency_en"]. "</strong>" . ' = ' . $value["selling_rate"] . " UZS\n";
            }
        }


        Telegram::sendMessage([
            "chat_id" => $chat_id,
            "text" => $text,
            "parse_mode" => "html"
        ]);
    }
}
