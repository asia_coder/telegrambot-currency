<?php

namespace App\Http\Controllers\Telegram\Buttons;

use App\Http\Controllers\Telegram\Card\CardController;
use App\Http\Controllers\Telegram\CommandController;
use App\Http\Controllers\Telegram\RouteController;
use App\Http\Controllers\Telegram\TelegramController;
use App\Models\Admin\AdminCard;
use App\Models\Card\Card;
use App\Models\Currency\Currency;
use App\Models\Data;
use App\Models\Order\Order;
use App\Models\State\State;
use App\Models\User\TelegramUser;
use App\Services\Keyboards\InlineKeyboard;
use App\Services\Keyboards\ReplyKeyboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class AdminController extends Controller
{
    public function __construct()
    {
        $user = TelegramUser::where('chat_id', Data::getChatId())
                            ->where('role', 'admin')
                            ->first();

        if (empty($user)) exit;
    }

    public function card($card_step)
    {
        switch ($card_step) {
            case 'admin_card_uzcard':
                $this->cardData('uzcard');
                break;

            case 'admin_card_qiwi':
                $this->cardData('qiwi');
                break;

            case 'admin_card_fio_uzcard':
                $this->editCardState('uzcard', 'fio');
                break;

            case 'admin_card_number_uzcard':
                $this->editCardState('uzcard', 'number');
                break;

            case 'admin_card_number_qiwi':
                $this->editCardState('qiwi', 'number');
                break;

        }
    }

    public function editCard($user_text)
    {
        try {
            $state = State::where('chat_id', Data::getChatId())->first()->toArray();
            $state = json_decode($state['state'], true);

            if (isset($state['admin']['column'])) {
                $column = $state['admin']['column'];
                $card = $state['admin']['card'];
                AdminCard::where('card', $card)->update([
                    $column => $user_text
                ]);

                $text = "Информация сохранена: <b>$user_text</b>";

                $stateNew = json_encode([
                    "admin" => [
                        "method" => "action"
                    ]
                ]);

                Data::setState($stateNew);

                Telegram::sendMessage([
                    "chat_id" => Data::getChatId(),
                    "text" => $text,
                    "parse_mode" => "html"
                ]);
            }
        }  catch(TelegramResponseException $e){
            return;
        }
    }

    protected function editCardState($card, $column)
    {
        $state = json_encode([
            "admin" => [
                "method" => "editCard",
                "column" => $column,
                "card" => $card
            ]
        ]);

        $text = "";
        if ($column === 'number') {
            $text = "Введите новый номер карты";
        } else {
            $text = "Введите ФИО";
        }

        Data::setState($state);

        Telegram::sendMessage([
            "chat_id" => Data::getChatId(),
            "text" => $text
        ]);
    }

    protected function cardData($card)
    {
        $buttons = [];
        $chat_id = Data::getChatId();
        $text = "Что бы изменить информацию нажмите на нужную кнопку";

        if ($card === "uzcard") {
            $buttons[] = [
                [
                    "text" => "Изменить ФИО ($card)",
                    "callback_data" => "admin_card_fio_$card"
                ]
            ];
        }

        $buttons[] = [
            [
                "text" => "Изменить номер карты ($card)",
                "callback_data" => "admin_card_number_$card"
            ]
        ];

        Telegram::deleteMessage([
            "chat_id" => $chat_id,
            "message_id" => Data::getMessageId()
        ]);

        InlineKeyboard::hideKeyboard(true);
        $msg = InlineKeyboard::emoji(false)->getKeyboard(
            $chat_id,
            $text,
            $buttons
        );

        try{
            Telegram::sendMessage($msg);
        } catch(TelegramResponseException $e){
            return;
        }
    }

    public function page()
    {
        $state = json_encode([
            "admin" => [
                "method" => "action"
            ]
        ]);

        $keyboard = Data::getAdminMenu();

        $text = 'staff';

        $msg = ReplyKeyboard::emoji(false)->hideKeyboard(false)->getKeyboard(
            Data::getChatId(),
            $text,
            $keyboard
        );

        try {
            Telegram::sendMessage($msg);
            Data::setState($state);
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }

    }

    public function action($text)
    {

        if($text == 'Курсы'){

            $currencies = Currency::all()->toArray();
            $buttons = [];
            $chat_id = Data::getChatId();
            $text = "Что бы изменить курс нажмите на нужную кнопку";

            foreach($currencies as $value){
                if($value['currency_en'] != 'UZCARD'){
                    $buttons[] = [
                        [
                            "text" => $value["currency_en"] . " sale - " . $value["selling_rate"],
                            "callback_data" => "change_currency_".$value["id"] . "_sell"
                        ],
                        [
                            "text" => $value["currency_en"] . " buy - " . $value["purchase_rate"],
                            "callback_data" => "change_currency_".$value["id"]. "_buy"
                        ]

                    ];
                }
            }

            InlineKeyboard::hideKeyboard(true);
            $msg = InlineKeyboard::emoji(false)->getKeyboard(
                $chat_id,
                $text,
                $buttons
            );

            try{
                Telegram::sendMessage($msg);
                //Data::setState();
            } catch(TelegramResponseException $e){
                return;
            }

            return;
        }
        elseif($text == 'Карты') {

            $adminCards = AdminCard::all()->toArray();
            $buttons = [];
            $chat_id = Data::getChatId();
            $text = "Что бы изменить информацию нажмите на нужную кнопку";

            foreach ($adminCards as $value) {
                $buttons[] = [
                    [
                        "text" => strtoupper($value['card']),
                        "callback_data" => "admin_card_$value[card]"
                    ]
                ];
            }


            InlineKeyboard::hideKeyboard(true);
            $msg = InlineKeyboard::emoji(false)->getKeyboard(
                $chat_id,
                $text,
                $buttons
            );

            try{
                Telegram::sendMessage($msg);
            } catch(TelegramResponseException $e){
                return;
            }

            return;
        }
        elseif($text == 'Резерв'){
            $currencies = Currency::all()->toArray();
            $buttons = [];
            $chat_id = Data::getChatId();
            $text = "Что бы изменить резерв нажмите на нужную кнопку";

            foreach($currencies as $value){
                $buttons[] = [
                    [
                        "text" => $value["currency_en"] . " резерв - " . $value["currency_amount"],
                        "callback_data" => "change_reserve_".$value["id"]
                    ]
                ];
            }

            InlineKeyboard::hideKeyboard(true);
            $msg = InlineKeyboard::emoji(false)->getKeyboard(
                $chat_id,
                $text,
                $buttons
            );

            try{
                Telegram::sendMessage($msg);
                //Data::setState();
            } catch(TelegramResponseException $e){
                return;
            }

            return;
        }
        elseif($text == 'Заявки'){
            $orders = Order::where('status', 'wait')->get();
            $chat_id = Data::getChatId();

            if($orders->count() == 0){
                Telegram::sendMessage([
                    'chat_id' => $chat_id,
                    'text'    => 'В данный момент нет необработанных заявок'
                ]);
                return;
            }

            // send to admin
            $admin = TelegramUser::where('role', 'admin')->where('chat_id', Data::getChatId())->first();
            foreach($orders as $orderUser) {
                try {
                    $user = TelegramUser::where('id', $orderUser->user_id)->first();
                    $currencyFrom = Currency::where('id', $orderUser->give_currency)->first();
                    $currencyTo = Currency::where('id', $orderUser->get_currency)->first();

                    $card_type = "";
                    switch ($currencyTo->currency_en) {
                        case Data::$UZCARD:
                            $card_type = "uzcard";
                            break;
                        case Data::$QIWI_USD:
                        case Data::$QIWI_RUB:
                            $card_type = "qiwi";
                            break;
                    }

                    $cards = Card::where('user_id', $user->id)->orderBy('type')->get();

                    if (!empty($cards) && isset($currencyFrom->currency_en) && isset($currencyTo->currency_en)) {
                        $date = date('d.m.Y H:i', strtotime($orderUser->updated_at));

                        $userCards = "";
                        foreach ($cards as $key => $card) {
                            $cardType = strtoupper($card->type);
                            $userCards .= "💳: $card->number ($cardType)\n";
                        }

                        $adminToMessage = "🆔 ID: $orderUser->id
👤: $user->name
☎️: +$user->phone
🔀: $currencyFrom->currency_en ➡️ $currencyTo->currency_en
{$userCards}📆: $date
💰: $orderUser->give_amount $currencyFrom->currency_en ➡️ $orderUser->get_amount $currencyTo->currency_en";

                        $buttons = [
                            [
                                [
                                    "text" => 'Одобрить',
                                    "callback_data" => "order_resolve_".$orderUser["id"]."_".$orderUser["user_id"]
                                ],
                                [
                                    "text" => 'Отказать',
                                    "callback_data" => "order_reject_".$orderUser["id"]."_".$orderUser["user_id"]
                                ]
                            ]
                        ];

                        $msgAdmin = InlineKeyboard::emoji(false)->getKeyboard(
                            $admin->chat_id,
                            $adminToMessage,
                            $buttons
                        );

                        usleep(30000);
                        Telegram::sendMessage($msgAdmin);
                    }

                } catch(TelegramResponseException $e){
                    return;
                }
            }

            return;
        }
    }


    public static function setCurrency($text)
    {
        if(is_numeric($text)){
            $state = json_decode(State::where('chat_id', TelegramController::$user->chat_id)->first()->state, true);
            $id = $state['admin']['id'];
            $type = $state['admin']['type'];
            $currency = Currency::find($id);

            if($type == "sell"){
                $currency->selling_rate = $text;
            }
            elseif($type == "buy"){
                $currency->purchase_rate = $text;
            }

            $currency->save();

            Telegram::sendMessage([
                "chat_id" => Data::getChatId(),
                "text" => "Курс изменен"
            ]);

            $state = json_encode([
                "admin" => [
                    "method" => "action"
                ]
            ]);

            Data::setState($state);
        }
        else{
            Telegram::sendMessage([
                "chat_id" => Data::getChatId(),
                "text" => "Не корректное значение"
            ]);
        }
    }

    public function setCurrencyAmount($text)
    {
        if(is_numeric($text)){
            $state = json_decode(State::where('chat_id', TelegramController::$user->chat_id)->first()->state, true);
            $id = $state['admin']['id'];
            $currency = Currency::find($id);
            $currency->currency_amount = $text;
            $currency->save();

            Telegram::sendMessage([
                "chat_id" => Data::getChatId(),
                "text" => "Резерв изменен"
            ]);

            $state = json_encode([
                "admin" => [
                    "method" => "action"
                ]
            ]);

            Data::setState($state);
        }
        else{
            Telegram::sendMessage([
                "chat_id" => Data::getChatId(),
                "text" => "Не корректное значение"
            ]);
        }
    }
}
