<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 2019-03-28
 * Time: 22:20
 */

namespace App\Http\Controllers\Telegram\Buttons;


use App\Http\Controllers\Controller;
use App\Models\Admin\AdminCard;
use App\Models\Card\Card;
use App\Models\Currency\Currency;
use App\Models\Data;
use App\Models\Order\Order;
use App\Models\State\State;
use App\Models\User\TelegramUser;
use App\Services\Keyboards\InlineKeyboard;
use App\Services\Keyboards\ReplyKeyboard;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class OrderController extends Controller
{
    protected static $min_amount_rub = 50;
    protected static $min_amount_usd = 10;
    protected static $min_amount_uzs = 1000;

    protected static $commission = 0;

    public function __construct()
    {
        $user = TelegramUser::where('chat_id', Data::getChatId())->first();
        $cards = Card::where('user_id', $user->id)->get();

        if (count($cards) < 2) {
            $msg = ReplyKeyboard::emoji(false)->getKeyboard(
                Data::getChatId(),
                Data::getUserConfig('cards_add'),
                Data::getMainMenu()
            );

            Telegram::sendMessage($msg);
            exit;
            return;
        }
    }

    public function page()
    {
        $buttons = [
            [
                [
                    "text" => "UZCARD ➡️ QIWI RUB",
                    "callback_data" => Data::getConvertData(
                        Data::$UZCARD,
                        Data::$QIWI_RUB
                    )
                ],
                [
                    "text" => "QIWI RUB ➡️ UZCARD",
                    "callback_data" => Data::getConvertData(
                        Data::$QIWI_RUB,
                        Data::$UZCARD
                    )
                ]
            ],
            /*[
                [
                    "text" => "UZCARD ➡️ QIWI USD",
                    "callback_data" => Data::getConvertData(
                        Data::$UZCARD,
                        Data::$QIWI_USD
                    )
                ],
                [
                    "text" => "QIWI USD ➡️ UZCARD",
                    "callback_data" => Data::getConvertData(
                        Data::$QIWI_USD,
                        Data::$UZCARD
                    )
                ]
            ],*/
        ];

        $text = Data::getUserConfig('order_line') . ":";

        $msg = InlineKeyboard::emoji(false)->getKeyboard(
            Data::getChatId(),
            $text,
            $buttons
        );

        try {
            Telegram::sendMessage($msg);
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }
    }


    public function run($order_step)
    {
        if (isset($order_step['currency_convert'])) {

            $this->createOrder($order_step['currency_convert']);

        } elseif (isset($order_step['cancel'])) {

            $this->deleteOrder($order_step['cancel']);

        } elseif (isset($order_step['state'])) {

            $this->stateWrite($order_step['state']);

        } elseif (isset($order_step['pay_button'])) {

            $this->confirmPay($order_step['pay_button']);

        } elseif (isset($order_step['pay_done'])) {

            $this->doneOrder($order_step['pay_done']['order_id']);

        }
    }

    protected function doneOrder($order_id)
    {
        try {
            $order = (new Order())->where('id', $order_id)->where('status', Order::$PAY)->update([
                "status" => Order::$WAIT
            ]);

            $text = "";
            if ($order) {

                if (Data::getUserLang() === 'uz') {
                    $text .= "Buyurtma ID-si: $order_id\n";
                    $text .= "Sana: " . date('d.m.Y H:i', time()) . "\n\n";
                    $text .= "<b>Sizning buyurtmangiz qabul qilindi.</b> Operator javobini kuting";
                } else {
                    $text .= "ID заявка: $order_id\n";
                    $text .= "Дата: " . date('d.m.Y H:i', time()) . "\n\n";
                    $text .= "<b>Ваша заявка передана на обработку.</b> Ожидайте уведомления";
                }

                $msg = ReplyKeyboard::emoji(false)->getKeyboard(
                    Data::getChatId(),
                    $text,
                    Data::getMainMenu()
                );

                Telegram::deleteMessage([
                    "chat_id" => Data::getChatId(),
                    "message_id" => Data::getMessageId()
                ]);

                Telegram::sendMessage($msg);

                // send to admin
                $orderUser = Order::where('id', $order_id)->first();
                $user = TelegramUser::where('id', $orderUser->user_id)->first();
                $currencyFrom = Currency::where('id', $orderUser->give_currency)->first();
                $currencyTo = Currency::where('id', $orderUser->get_currency)->first();

                $card_type = "";
                switch ($currencyTo->currency_en) {
                    case Data::$UZCARD:
                        $card_type = "uzcard";
                        break;
                    case Data::$QIWI_USD:
                    case Data::$QIWI_RUB:
                        $card_type = "qiwi";
                        break;
                }

                $cards = Card::where('user_id', $user->id)->orderBy('type')->get();

                $admins = TelegramUser::where('role', 'admin')->get();

                if (!empty($admins) && !empty($cards) && isset($currencyFrom->currency_en) && isset($currencyTo->currency_en)) {
                    $date = date('d.m.Y H:i', strtotime($orderUser->updated_at));

                    $userCards = "";
                    foreach ($cards as $card) {
                        $cardType = strtoupper($card->type);
                        $userCards .= "💳: $card->number ($cardType)\n";
                    }

                    $adminToMessage = "🆔 ID: $orderUser->id
👤: $user->name
☎️: +$user->phone
🔀: $currencyFrom->currency_en ➡️ $currencyTo->currency_en
{$userCards}📆: $date
💰: $orderUser->give_amount $currencyFrom->currency_en ➡️ $orderUser->get_amount $currencyTo->currency_en";

                    $buttons = [
                        [
                            [
                                "text" => 'Одобрить',
                                "callback_data" => "order_resolve_".$orderUser["id"]."_".$orderUser["user_id"]
                            ],
                            [
                                "text" => 'Отказать',
                                "callback_data" => "order_reject_".$orderUser["id"]."_".$orderUser["user_id"]
                            ]
                        ]
                    ];

                    foreach ($admins as $admin) {
                        $msgAdmin = InlineKeyboard::emoji(false)->getKeyboard(
                            $admin->chat_id,
                            $adminToMessage,
                            $buttons
                        );

                        usleep(20000);
                        Telegram::sendMessage($msgAdmin);
                    }
                }

                return;
            }
        } catch (TelegramResponseException $e) {
            return info($e);
        }
    }

    protected function confirmPay($confirm)
    {
        try {
            $order = Order::where('id', $confirm['order_id'])->first();
            if (isset($order) && empty($order)) {
                Telegram::deleteMessage([
                    "chat_id" => Data::getChatId(),
                    "message_id" => Data::getMessageId()
                ]);

                $msg = ReplyKeyboard::emoji(false)->getKeyboard(
                    Data::getChatId(),
                    Data::getUserConfig('order_deleted'),
                    Data::getMainMenu()
                );

                Telegram::sendMessage($msg);
                return;
            }

            $currencyTo = Currency::where('id', $order->get_currency)->first();
            $currencyFrom = Currency::where('id', $order->give_currency)->first();

            $price_order = ($order->get_amount * 100) / (100 - self::$commission);

            if ($currencyTo->currency_amount >= $price_order) {

                // reserve -
                $reserve = $currencyTo->currency_amount - $order->get_amount;
                $currencyTo->currency_amount = $reserve;
                $currencyTo->save();

                $reserve = 0;
                // reserve +
                $reserve = $currencyFrom->currency_amount + $order->give_amount;
                $currencyFrom->currency_amount = $reserve;
                $currencyFrom->save();

                $order->status = Order::$PAY;
                $order->save();
            }

            switch ($confirm['confirm']) {
                case Data::$UZCARD:
                    $this->uzcardConfirm($confirm['order_id']);
                    break;

                case Data::$QIWI_RUB:
                case Data::$QIWI_USD:
                    $this->qiwiConfirm($confirm['order_id']);
                    break;

                default:
                    break;
            }
        } catch (TelegramResponseException $e) {
            return info($e);
        }
    }

    protected function qiwiConfirm(&$order_id)
    {
        try {
            $currency_en = "";
            $order = (new Order())->where('id', $order_id)->first();
            $currency_en = (new Currency())->where('id', $order->give_currency)->pluck('currency_en')->first();

            $text = "";
            $currencyName = "USD";
            if (isset($currency_en) && $currency_en === Data::$QIWI_RUB) {
                $currencyName = "RUB";
            }

            $adminCard = AdminCard::where('card', 'qiwi')->first()->toArray();

            if (Data::getUserLang() === 'uz') {
                $text .= "<b>Almashuvingiz muvaffaqiyatli bajarilishi uchun quyidagi harakatlarni amalga oshiring:</b>\n\n";
                $text .= "- QIWI.COM to'lov tizimiga kiring\n";
                $text .= "- Pastda ko'rsatilgan to'lov miqdorini <b>$adminCard[number]</b> hamyon raqamiga o'tkazing\n\n";
                $text .= "- «Men to'lovni amalga oshirdim» tugmasini bosing\n\n";
                $text .= "Operator tomonidan almashuv tasdiqlanishini kuting\n\n";
                $text .= "To'lov miqdori: $order->give_amount $currencyName\n\n";
                $text .= "Ushbu almashuv operator tomonidan amalga oshiriladi va ish vaqtida 
<b>5 daqiqadan 3 soatgacha</b> davom etadi";
            } else {
                $text .= "<b>Для успешной обработки вашей заявки пожалуйста выполните следующие действия:</b>\n\n";
                $text .= "- Авторизуйтесь в платежной системе QIWI.COM\n";
                $text .= "- Переведите указанную ниже сумму на кошелек\n <b>$adminCard[number]</b>\n";
                $text .= "- Нажмите на кнопку «Я оплатил заявку»\n\n";
                $text .= "Ожидайте обработку заявки оператором\n\n";
                $text .= "Сумма платежа: $order->give_amount $currencyName\n\n";
                $text .= "Данная операция производится оператором в ручном 
режиме и занимает <b>от 5 минут до 3 часа</b> в рабочее время";
            }

            $keyboard = [
                [
                    [
                        'text' => Data::getUserConfig('pay_confirm'),
                        'callback_data' => json_encode([
                            'order' => [
                                'pay_done' => [
                                    'order_id' => $order->id
                                ]
                            ]
                        ])
                    ]
                ],
                [
                    [
                        "text" => Data::getUserConfig('cancel'),
                        "callback_data" => json_encode([
                            "order" => [
                                "cancel" => $order->id
                            ]
                        ])
                    ]
                ]
            ];

            $msg = InlineKeyboard::emoji(false)->getKeyBoard(
                Data::getChatId(),
                $text,
                $keyboard
            );

            Telegram::deleteMessage([
                "chat_id" => Data::getChatId(),
                "message_id" => Data::getMessageId()
            ]);

            return Telegram::sendMessage($msg);

        } catch (TelegramResponseException $e) {
            return info($e);
        }
    }

    protected function uzcardConfirm(&$order_id)
    {
        try {
            $order = (new Order())->where('id', $order_id)->first();

            $text = "";

            $adminCard = AdminCard::where('card', 'uzcard')->first()->toArray();

            if (Data::getUserLang() === 'uz') {
                $text .= "<b>Almashuvingiz muvaffaqiyatli bajarilishi uchun quyidagi harakatlarni amalga oshiring:</b>\n\n";
                $text .= "- PAYME.UZ to'lov tizimiga kiring\n<i>\n* To'lov faqat PAYME.UZ to'lov tizimidan qabul qilinadi. Boshqa tizimlardan to'lov qabul qilinmaydi.</i>\n\n";
                $text .= "- Pastda ko'rsatilgan to'lov miqdorini <b>$adminCard[number] ($adminCard[fio])</b> karta raqamiga o'tkazing\n\n";
                $text .= "- «Men to'lovni amalga oshirdim» tugmasini bosing\n\n";
                $text .= "Operator tomonidan almashuv tasdiqlanishini kuting\n\n";
                $text .= "To'lov miqdori: $order->give_amount UZS\n\n";
                $text .= "Ushbu almashuv operator tomonidan amalga oshiriladi va ish vaqtida 
<b>5 daqiqadan 3 soatgacha</b> davom etadi";
            } else {
                $text .= "<b>Для успешной обработки вашей заявки пожалуйста выполните следующие действия:</b>\n\n";
                $text .= "- Авторизуйтесь в платежной системе PAYME.UZ\n";
                $text .= "- Переведите указанную ниже сумму на карточку\n <b>$adminCard[number] ($adminCard[fio])</b>\n<i>\n* Платежи принимаются только через систему платежа PAYME.UZ. Платежи через другие системы не принимаются.</i>\n\n";
                $text .= "- Нажмите на кнопку «Я оплатил заявку»\n\n";
                $text .= "Ожидайте обработку заявки оператором\n\n";
                $text .= "Сумма платежа: $order->give_amount UZS\n\n";
                $text .= "Данная операция производится оператором в ручном 
режиме и занимает <b>от 5 минут до 3 часа</b> в рабочее время";
            }

            $keyboard = [
                [
                    [
                        'text' => Data::getUserConfig('pay_confirm'),
                        'callback_data' => json_encode([
                            'order' => [
                                'pay_done' => [
                                    'order_id' => $order->id
                                ]
                            ]
                        ])
                    ]
                ],
                [
                    [
                        "text" => Data::getUserConfig('cancel'),
                        "callback_data" => json_encode([
                            "order" => [
                                "cancel" => $order->id
                            ]
                        ])
                    ]
                ]
            ];

            $msg = InlineKeyboard::emoji(false)->getKeyBoard(
                Data::getChatId(),
                $text,
                $keyboard
            );

            Telegram::deleteMessage([
                "chat_id" => Data::getChatId(),
                "message_id" => Data::getMessageId()
            ]);

            return Telegram::sendMessage($msg);

        } catch (TelegramResponseException $e) {
            return info($e);
        }
    }

    public function validAmount($user_amount)
    {

        try {

            if (!is_numeric($user_amount)) {
                return Telegram::sendMessage([
                    'chat_id' => Data::getChatId(),
                    'text' => Data::getUserConfig('error_request')
                ]);
            }

            $state_result = State::where('chat_id', Data::getChatId())->where('state','!=','')->first();

            if (empty($state_result)) return;

            $state_array = json_decode($state_result['state'], true);

            $order = (new Order())->where('id', $state_array['order']['order_id'])->first();
            $currencyFrom = (new Currency())->where('id', $order->give_currency)->first();
            $currencyTo = (new Currency())->where('id', $order->get_currency)->first();

            $currencyName = "";
            $min_amount = 0;

            switch ($currencyFrom->currency_en) {
                case Data::$QIWI_RUB:
                    $currencyName = "RUB";
                    $min_amount = self::$min_amount_rub;
                    break;

                case Data::$QIWI_USD:
                    $currencyName = "USD";
                    $min_amount = self::$min_amount_usd;
                    break;

                case Data::$UZCARD:
                    $currencyName = "UZS";
                    $min_amount = self::$min_amount_uzs;
                    break;

                default:
                    break;
            }

            if ($min_amount <= $user_amount) {
                $persent = 100 - self::$commission;

                $price_order = 0;

                // проверка на пакупка или продажа валюты
                if ($currencyFrom->currency_en === Data::$UZCARD) {
                    $price_order = round($user_amount / $currencyTo->selling_rate, 2);

                    if ($price_order > $currencyTo->currency_amount) {
                        $text = Data::getUserConfig('reserve_not_money');
                        $msg = ReplyKeyboard::emoji(false)->getKeyBoard(
                            Data::getChatId(),
                            $text,
                            Data::getMainMenu()
                        );

                        Telegram::sendMessage($msg);
                        Data::setState();
                        return;
                    }

                } else {
                    $price_order = round($currencyFrom->purchase_rate * $user_amount, 2);

                    if ($price_order > $currencyTo->currency_amount) {
                        $text = Data::getUserConfig('reserve_not_money');
                        $msg = ReplyKeyboard::emoji(false)->getKeyBoard(
                            Data::getChatId(),
                            $text,
                            Data::getMainMenu()
                        );

                        Telegram::sendMessage($msg);
                        Data::setState();
                        return;
                    }
                }

                $price_order = round(($price_order * $persent) / 100, 2);
                $order->give_amount = $user_amount;
                $order->get_amount = $price_order;
                $order->save();

                $toCurrency = "";
                switch ($currencyTo->currency_en) {
                    case Data::$QIWI_RUB:
                        $toCurrency = "RUB";
                        break;

                    case Data::$QIWI_USD:
                        $toCurrency = "USD";
                        break;

                    case Data::$UZCARD:
                        $toCurrency = "UZS";
                        break;

                    default:
                        break;
                }

                $fromCurrency = "";
                switch ($currencyFrom->currency_en) {
                    case Data::$QIWI_RUB:
                        $fromCurrency = "RUB";
                        break;

                    case Data::$QIWI_USD:
                        $fromCurrency = "USD";
                        break;

                    case Data::$UZCARD:
                        $fromCurrency = "UZS";
                        break;

                    default:
                        break;
                }

                $cardQiwi = Card::where('user_id', $order->user_id)->where('type', 'qiwi')->first();
                $cardUzcard = Card::where('user_id', $order->user_id)->where('type', 'uzcard')->first();

                $commission = self::$commission;
                if (Data::getUserLang() === "uz") {
                    $text = "Sizning buyurtmangiz:
<b>$currencyFrom->currency_en</b> ➡️ <b>$currencyTo->currency_en</b>
Buyurtma <b>ID</b>-si: <b>$order->id</b>
Berish: <b>$user_amount $fromCurrency</b>
Olish: <b>$price_order $toCurrency</b>
UzCard karta: <b>$cardUzcard[number]</b>
QIWI hamyon: <b>$cardQiwi[number]</b>
<b>*To'lov tizimi komissiyasi $commission%</b>
";
                } else {
                    $text = "Ваша заявка на обмен:
<b>$currencyFrom->currency_en</b> ➡️ <b>$currencyTo->currency_en</b>
<b>ID</b> заявки: <b>$order->id</b>
Отдаете: <b>$user_amount $fromCurrency</b>
Получаете: <b>$price_order $toCurrency*</b>
Ваша карта: <b>$cardUzcard[number]</b>
Ваш QIWI: <b>$cardQiwi[number]</b>
<b>*Комиссия платёжной системы $commission%</b>
";
                }

                $keyboard = [
                    [
                        [
                            'text' => Data::getUserConfig('pay_button'),
                            'callback_data' => json_encode([
                                'order' => [
                                    'pay_button' => [
                                        'confirm' => $currencyFrom->currency_en,
                                        'order_id' => $order->id
                                    ]
                                ]
                            ])
                        ]
                    ],
                    [
                        [
                            "text" => Data::getUserConfig('cancel'),
                            "callback_data" => json_encode([
                                "order" => [
                                    "cancel" => $order->id
                                ]
                            ])
                        ]
                    ]
                ];

                $msg = InlineKeyboard::emoji(false)->getKeyBoard(
                    Data::getChatId(),
                    $text,
                    $keyboard
                );

                Telegram::sendMessage($msg);
                Data::setState();
                return;
            } else {
                return Telegram::sendMessage([
                    'chat_id' => Data::getChatId(),
                    'parse_mode' => 'html',
                    'text' => Data::getUserConfig('minimum') . ': <b>' . $min_amount . ' ' . $currencyName . '</b>'
                ]);
            }

        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }
    }

    protected function stateWrite($state)
    {

        try {
            if (isset($state) && !empty($state)) {
                Data::setState(json_encode([
                    'order' => $state
                ]));

                $order = (new Order())->where('id', $state['order_id'])->first();
                $currency = (new Currency())->where('id', $order->give_currency)->first();

                $min_amount = "";
                $currencyName = "";
                switch ($currency->currency_en) {
                    case Data::$QIWI_RUB:
                        $currencyName = "RUB";
                        $min_amount = self::$min_amount_rub . " " . $currencyName;
                        break;

                    case Data::$QIWI_USD:
                        $currencyName = "USD";
                        $min_amount = self::$min_amount_usd . " " . $currencyName;
                        break;

                    case Data::$UZCARD:
                        $currencyName = "UZS";
                        $min_amount = self::$min_amount_uzs . " " . $currencyName;
                        break;

                    default:
                        break;
                }

                if (Data::getUserLang() === "uz") {
                    $text = "Sizga kerakli miqdorni <b>$currencyName</b> da kiriting
Kamida: <b>$min_amount</b>
";
                } else {
                    $text = "Напишите нужную вам сумму в <b>$currencyName</b>
Минимум: <b>$min_amount</b>
";
                }

                $msg = ReplyKeyboard::emoji(false)->getKeyboard(
                    Data::getChatId(),
                    $text,
                    Data::getMainMenu()
                );

                Telegram::deleteMessage([
                    "chat_id" => Data::getChatId(),
                    "message_id" => Data::getMessageId()
                ]);

                Telegram::sendMessage($msg);
            }
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }
    }

    protected function createOrder($order)
    {
        $user_order = new Order();
        $currency = new Currency();
        $user = (new TelegramUser())->where('chat_id', Data::getChatId())->first();

        $give = $currency->where('currency_en', $order['from'])->pluck('id')->first();
        $get = $currency->where('currency_en', $order['to'])->pluck('id')->first();
        $cardQiwi = Card::where('user_id', $user->id)->where('type', 'qiwi')->first();
        $cardUzcard = Card::where('user_id', $user->id)->where('type', 'uzcard')->first();

        try {

            if (empty($user)) {
                return;
            }

            $order_id = 0;
            $user_order->user_id = $user->id;
            $user_order->give_currency = $give;
            $user_order->get_currency = $get;
            $user_order->save();

            ///
            ///
            $to = $order['to'] === Data::$UZCARD ? 'UZS' : $order['to'];
            if (Data::getUserLang() === "uz") {
                $text = '<b>'.Data::getUserConfig('order_process') . "</b>\n";
                $text .= "
Sizning buyurtmangiz:
<b>$order[from]</b> ➡️ <b>$order[to]</b>
Buyurtma <b>ID</b>-si: <b>$user_order->id</b>
Berish: <b>$order[from]</b><i>-da kiriting</i>
Olish: ? <b>$to</b>
UzCard karta: <b>$cardUzcard[number]</b>
QIWI hamyon: <b>$cardQiwi[number]</b>
";
            } else {
                $text = '<b>'.Data::getUserConfig('order_process') . "</b>\n";
                $text .= "
Ваша заявка на обмен:
<b>$order[from]</b> ➡️ <b>$order[to]</b>
<b>ID</b> заявки: <b>$user_order->id</b>
Отдаете: <i>введите</i> <b>$order[from]</b>
Получаете: ? <b>$to</b>
Ваша карта: <b>$cardUzcard[number]</b>
Ваш QIWI: <b>$cardQiwi[number]</b>
";
            }

            $currencyUser = "";
            if ($order['from'] !== Data::$QIWI_RUB) {
                $currencyUser = "$order[from] 🇺🇿";
            } else {
                $currencyUser = "$order[from] 🇷🇺";
            }

            $keyboard = [
                [
                    [
                        "text" => Data::getUserConfig('order_sum') . $currencyUser,
                        "callback_data" => json_encode([
                            "order" => [
                                "state" => [
                                    "method" => "validAmount",
                                    "order_id" => $user_order->id
                                ]
                            ]
                        ])
                    ]
                ],
                [
                    [
                        "text" => Data::getUserConfig('cancel'),
                        "callback_data" => json_encode([
                            "order" => [
                                "cancel" => $user_order->id
                            ]
                        ])
                    ]
                ]
            ];

            $msg = InlineKeyboard::emoji(false)->getKeyboard(
                Data::getChatId(),
                $text,
                $keyboard
            );

            Telegram::deleteMessage([
                "chat_id" => Data::getChatId(),
                "message_id" => Data::getMessageId()
            ]);
            Telegram::sendMessage($msg);

        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }

    }

    protected function deleteOrder($order_id)
    {
        try {
            $user = new TelegramUser();
            $user = $user->where('chat_id', Data::getChatId())->first();

            if (!empty($user)) {
                $order = new Order();
                $deleteOrder = $order->where('user_id', $user->id)->where('id', $order_id)->first();

                if ($deleteOrder->status === 'pay') {
                    // reserve -
                    $reserveMinus = Currency::where('id', $deleteOrder->give_currency)->first();
                    $reserveMinus->currency_amount = $reserveMinus->currency_amount - $deleteOrder->give_amount;
                    $reserveMinus->save();

                    // reserve +
                    $reserve = Currency::where('id', $deleteOrder->get_currency)->first();
                    $reserve->currency_amount = $reserve->currency_amount + $deleteOrder->get_amount;
                    $reserve->save();
                }

                $delete_result = $deleteOrder->delete();

                if ($delete_result) {
                    $text = Data::getUserConfig('cancel_order');

                    $msg = ReplyKeyboard::emoji(false)->getKeyboard(
                        Data::getChatId(),
                        $text,
                        Data::getMainMenu()
                    );

                    Telegram::deleteMessage([
                        "chat_id" => Data::getChatId(),
                        "message_id" => Data::getMessageId()
                    ]);
                    Telegram::sendMessage($msg);
                }
            }
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }

    }
}
