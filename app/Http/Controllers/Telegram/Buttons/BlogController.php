<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 2019-04-01
 * Time: 02:22
 */

namespace App\Http\Controllers\Telegram\Buttons;


use App\Http\Controllers\Controller;
use App\Models\Data;
use App\Services\Keyboards\InlineKeyboard;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class BlogController extends Controller
{
    public function page()
    {
        $buttons = [
            [
                [
                    "text" => Data::getUserConfig('blog_button'),
                    "url" => "https://t.me/crossmarketing_uz"
                ],
                [
                    "text" => Data::getUserConfig('admin_page'),
                    "url" => "https://t.me/student6493"
                ]
            ]
        ];

        $text = Data::getUserConfig('blog_text') . ":";

        $msg = InlineKeyboard::emoji(false)->getKeyboard(
            Data::getChatId(),
            $text,
            $buttons
        );

        try {
            Telegram::sendMessage($msg);
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }
    }
}
