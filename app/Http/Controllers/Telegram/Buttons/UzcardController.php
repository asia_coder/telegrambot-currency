<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 2019-03-26
 * Time: 23:52
 */

namespace App\Http\Controllers\Telegram\Buttons;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Telegram\Card\CardController;
use App\Models\Card\Card;
use App\Models\Data;
use App\Models\User\TelegramUser;
use App\Services\Keyboards\ReplyKeyboard;
use Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\Laravel\Facades\Telegram;

class UzcardController extends Controller
{
    public function page()
    {
        $state = json_encode([
            "uzcard" => [
                "method" => "uzCardValidator"
            ]
        ]);

        $cards = Data::getCards();
        $keyboard = [
            [
                $cards['qiwi'],
                $cards['uzcard']
            ],
            [
                Data::getMenuButton()
            ]
        ];

        $text = Data::getUserConfig('uzcard_text_number');

        $msg = ReplyKeyboard::emoji(false)->getKeyboard(
            Data::getChatId(),
            $text,
            $keyboard
        );

        try {
            Telegram::sendMessage($msg);
            Data::setState($state);
        } catch (TelegramResponseException $e) {
            info($e);
        }
    }

    public function uzCardValidator(string $user_request)
    {
        $regexUzCard = Data::getUzCardPattern();

        $cards = Data::getCards();
        $keyboard = [
            [
                $cards['qiwi'],
                $cards['uzcard']
            ],
            [
                Data::getMenuButton()
            ]
        ];

        $text = Data::getUserConfig('error_request');

        if ( $isCorrect = (preg_match($regexUzCard, $user_request) == 1) ) {
            $text = Data::getUserConfig('uzcard_text_save');
        }

        $msg = ReplyKeyboard::emoji(false)->hideKeyboard()->getKeyboard(
            Data::getChatId(),
            $text,
            $keyboard
        );

        try {
            if ($isCorrect) {
                $userObject = new TelegramUser();
                $user = $userObject->where('chat_id', Data::getChatId())->first();

                $cardObject = new Card();
                $uzCard = $cardObject->where('user_id', $user->id)
                     ->where('type', 'uzcard')
                     ->first();

                $card = new CardController();
                if (empty($uzCard)) {
                    $card->store($user, [
                        "type" => "uzcard",
                        "number" => $user_request
                    ]);
                } else {
                    $card->update($user, $uzCard, [
                        "number" => $user_request
                    ]);
                }

                Data::setState();
            }

            Telegram::sendMessage($msg);
        } catch (TelegramResponseException $e) {
            info($e);
            return;
        }
    }
}
