<?php

namespace App\Http\Controllers\Telegram;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Exceptions\TelegramOtherException;
use Telegram\Bot\Exceptions\TelegramResponseException;

class CommandController extends Controller
{
    public static function init($hook, $needAuth = false)
    {
        try{
            if($needAuth){
                $commandclass = "\App\Commands\\Start";
                $commandObject = new $commandclass;
                $commandObject->init($hook);
            }
            else{
                $commandText = $hook['message']['text'];
                $commandclass = "\App\Commands\\".ucfirst(mb_substr($commandText, 1));

                if ( class_exists($commandclass) ) {
                    $commandObject = new $commandclass;
                    $commandObject->init($hook);
                }
                else{
                    info("Command not exists");
                    return;
                }
            }

        }
        catch(TelegramResponseException $e){
            return;
        }
    }
}
