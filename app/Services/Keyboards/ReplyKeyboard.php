<?php
/**
 * Created by PhpStorm.
 * User: dilshod
 * Date: 2019-03-22
 * Time: 00:54
 */

namespace App\Services\Keyboards;


use Telegram\Bot\Laravel\Facades\Telegram;

class ReplyKeyboard extends Keyboard
{
   // private static $hideKeyboard = false;

   // public static function hideKeyboard()
   // {
       // self::$hideKeyboard = true;
       // return new static();
   // }

    protected static function keyboardType()
    {
        $keyboard = static::$keyboard;
        $replyKeyboard = [
            'keyboard' => $keyboard,
            'resize_keyboard' => true
        ];

        /*if (self::$hideKeyboard) {
            array_merge([
                'hide_keyboard' => true,
                'selective' => false
            ], $replyKeyboard);
        } else {
            array_merge([
                'keyboard' => $keyboard
            ], $replyKeyboard);
        }*/

        $replyMarkup = Telegram::replyKeyboardMarkup($replyKeyboard);

        $msg = [
            'chat_id' => static::$chatId,
            'text' => static::$text,
            'reply_markup' => $replyMarkup,
            'parse_mode' => 'html'
        ];

        return $msg;
    }
}
